---
swagger: "2.0"
info:
  description: |
    ## Use ISS current location API to show location on a map.

    This is a Murano Service integration with
    ISS current location (http://open-notify.org/Open-Notify-API/ISS-Location-Now/) to allow your Murano solution
    to get location coordinates.
  version: "0.1"
  title: ISS Current Location  Integration
  contact:
    name: Tristan Harling
    email: tristan.harling@ensg.eu
host: api.open-notify.org
basePath: /
tags:
- name: Integration
  description: Interface to communicate to ISS service, for the ISS current location service
schemes:
- http
produces:
- application/json
paths:
  /iss-now.json:
    get:
      tags:
      - calls
      summary: Retrieve location of ISS
      description: Using this path, will get a json file containing timestamp, and location (Lat/Long) of the ISS.
      operationId: getLocation
      produces:
      - application/json
      parameters: []
      responses:
        200:
          description: success
          headers:
            Access-Control-Allow-Origin:
              type: string
          schema:
            type: object
            properties:
              iss_position:
                $ref: '#/definitions/inline_response_200_iss_position'
              message:
                type: string
                description: success or not
              timestamp:
                type: integer
                description: the time location timestamp where coordinates were valid
            description: location and timestamp results
        400:
          description: Bad request. User ID must be an integer and bigger than 0.
        401:
          description: Authorization information is missing or invalid.
        404:
          description: A user with the specified ID was not found.
        default:
          description: error
          schema:
            $ref: '#/definitions/ErrorResponse'
      x-exosite-example: |
        --#ENDPOINT GET /iss-now

        Iss_data = Iss-now()

        print(to_json(Iss_data))

        response.message = Iss_data
definitions:
  ErrorResponse:
    type: object
    properties:
      error:
        type: string
        description: Error message
      status:
        type: integer
        description: Response code
      type:
        type: string
        description: Error type
    description: Request error
  inline_response_200_iss_position:
    properties:
      latitude:
        type: string
        description: latitude
      longitude:
        type: string
        description: longitude
    description: the Iss coordinates
  inline_response_200:
    properties:
      iss_position:
        $ref: '#/definitions/inline_response_200_iss_position'
      message:
        type: string
        description: success or not
      timestamp:
        type: integer
        description: the time location timestamp where coordinates were valid
    description: location and timestamp results
externalDocs:
  description: ISS current location documentation
  url: https://open-notify.org/Open-Notify-API/ISS-Location-Now/
