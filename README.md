# ISS location Murano Integer API

Swagger files to communicate with a Murano App, using API service of WhereTheISS

https://wheretheiss.at/w/developer. 


**NOTE**
It can also use this API but not securised:
source of ISS current location
http://open-notify.org/Open-Notify-API/ISS-Location-Now/
